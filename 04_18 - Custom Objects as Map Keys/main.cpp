#include <iostream>
#include <map>
#include <string>

class Person {
private:
	std::string name;
	int age;
public:
	// Default constructor
	Person() : name(""), age(0) {}
	// Copy constructor
	Person(const Person &other) {
		name = other.name;
		age = other.age;
	}
	// Parameterised constructor
	Person(std::string name, int age) : name(name), age(age) {}

	void print() const {
		std::cout << name << ": " << age << std::flush;
	}

	// Overload the < operator. Required for using custom objects as key.
	// const Person because Person should not be changed.
	// const function because the operator should not change the object it's part of.
	// Both consts are required by the compiler.
	// Reference to Person for efficiency.
	bool operator<(const Person &other) const {
		if (name == other.name) {
			return age < other.age;
		}
		else {
			return name < other.name;
		}
	}
};

int main()
{
	std::map<Person, int> people;

	// Call user-defined constructor
	people[Person("Mike", 40)] = 40;

	// This updates Mike.
	//people[Person("Mike", 40)] = 123;

	// If the overloaded < operator compares only on name then this won't change the key, but it will update the value.
	// If the overloaded < operator compares on age where name is equal then this will add a new record into the map.
	people[Person("Mike", 444)] = 123;

	people[Person("Sue", 30)] = 30;
	people[Person("Raj", 20)] = 20;

	for (std::map<Person, int>::iterator it = people.begin(); it != people.end(); it++) {
		std::cout << it->second << ": " << std::flush;
		it->first.print();
		std::cout << std::endl;
	}

	std::cin.get();
	return 0;
}