#include <iostream>
#include <exception>

void goesWrong()
{
	bool error1Detected = false;
	bool error2Detected = false;
	bool error3Detected = true;

	if (error1Detected) {
		throw std::bad_alloc();
	}

	if (error2Detected) {
		throw std::exception();
	}

	if (error3Detected) {
		throw std::bad_array_new_length();
	}
}

int main()
{
	// NOTE: Remember to catch the exception subclasses first, otherwise we
	// will always catch 'exception' first! This is because of polymorphism.
	// (Note: bad_alloc is a subclass [type] of exception, but exception is
	// not a subclass [type] of bad_alloc.)
	try {
		goesWrong();
	}
	// bad_alloc is a subclass of exception, so if a bad_alloc is thrown then this
	// will be picked up before 'exception' because it is being caught first.
	catch (std::bad_alloc &e) {
		std::cout << "Catching bad_alloc: " << e.what() << std::endl;
	}
	// exception is the parent of all exceptions, so catching this will catch
	// every type of exception, and no other exceptions handled later in this try/catch
	// will ever be executed.
	catch (std::exception &e) {
		std::cout << "Catching exception: " << e.what() << std::endl;
	}
	// bad_array_new_length is a subclass of bad_alloc, so if error3Detected is true
	// and the others false we should expect to catch a bad_alloc.
	catch (std::bad_array_new_length &e) {
		std::cout << "Catching bad_array_new_length: " << e.what() << std::endl;
	}

	std::cin.get();
	return 0;
}