#include "Complex.h"

namespace caveofprogramming {

	std::ostream &operator<<(std::ostream &out, const Complex &rhs) {
		out << "(" << rhs.getReal() << ", " << rhs.getImaginary() << ")";
		return out;
	}

	Complex operator+(const Complex &lhs, const Complex &rhs) {
		return Complex(lhs.getReal() + rhs.getReal(), lhs.getImaginary() + rhs.getImaginary());
	}

	Complex operator+(const Complex &lhs, double d) {
		return Complex(lhs.getReal() + d, lhs.getImaginary());
	}

	Complex operator+(double d, const Complex &rhs) {
		return Complex(d + rhs.getReal(), rhs.getImaginary());
	}


	Complex::Complex() : real(0), imaginary(0)
	{
	}

	Complex Complex::operator*() const {
		return Complex(real, -imaginary);
	}

	Complex::Complex(double real, double imaginary) : real(real), imaginary(imaginary)
	{

	}

	Complex::Complex(const Complex &rhs)
	{
		real = rhs.real;
		imaginary = rhs.imaginary;
	}

	const Complex &Complex::operator=(const Complex &rhs)
	{
		real = rhs.real;
		imaginary = rhs.imaginary;
		return *this;
	}

	bool Complex::operator==(const Complex &rhs) const {
		return (real == rhs.real) && (imaginary == rhs.imaginary);
	}

	bool Complex::operator!=(const Complex &rhs) const {
		return !(*this == rhs); // Note that this is using the overloaded '==' operator above.
	}


}