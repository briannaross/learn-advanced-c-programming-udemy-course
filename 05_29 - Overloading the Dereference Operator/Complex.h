#pragma once

#include <iostream>

namespace caveofprogramming {

class Complex
{
private:
	double real;
	double imaginary;
public:
	Complex();
	Complex(double real, double imaginary);
	Complex(const Complex &rhs);
	const Complex &operator=(const Complex &rhs); // RoT 1
	double getReal() const { return real; }
	double getImaginary() const { return imaginary; }
	bool operator==(const Complex &rhs) const;
	bool operator!=(const Complex &rhs) const;
	Complex operator*() const;
};

std::ostream &operator<<(std::ostream &out, const Complex &rhs);
Complex operator+(const Complex &lhs, const Complex &rhs); // Rot 4a
Complex operator+(const Complex &lhs, double d); // Rot 4a
Complex operator+(double d, const Complex &rhs); // Rot 4a

}