#include <iostream>
#include "Complex.h"

using namespace caveofprogramming;

int main()
{
	Complex c1(2, 4);

	std::cout << *c1 + *Complex(4, 3) << std::endl; // Displays the conjugate

	std::cin.get();
	return 0;
}
