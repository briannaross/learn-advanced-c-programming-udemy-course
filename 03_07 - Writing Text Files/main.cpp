#include <iostream>
#include <fstream>
#include <string>

int main()
{
	//std::ofstream outFile;
	std::fstream outFile;
	std::string outputFileName = "test.txt";
	std::string strArr[10] = { "Bananas", "Apples", "Carrots", "Lettuce", "Cheese", "Milk", "Eggs", "Orange", "Bread", "Peas" };

	//outFile.open(outputFileName);
	outFile.open(outputFileName, std::ios::out);

	if (outFile.is_open()) {
		outFile << "Shopping List" << std::endl;
		outFile << "=============" << std::endl;
		for (int i = 1; i <= 10; i++) {
			outFile << i << ": " << strArr[i - 1] << std::endl;
		}
		outFile.close();
	}
	else {
		std::cout << "Could not create file: " << outputFileName << std::endl;
	}

	return 0;
}