#include <iostream>
#include <fstream>
#include <string>

int main()
{
	std::string filename = "stats.txt";
	std::ifstream inFile;

	inFile.open(filename);

	if (!inFile.is_open()) {
		return 1;
	}

	while (inFile) {
		std::string line;
		/*
		*** METHOD 1 ***
		getline(input, line, ':'); // Get characters up to a colon but not including it.

		int population;
		input >> population;

		// *** Pre C++11 ***
		// Discard next character. In this case it should discard the newline character.
		//input.get();
		// *** C++11 ***
		// Skip white space in the stream.
		input >> std::ws;

		// Break the loop if input is in an error state (eg. if we're on an empty line).
		if (!input) {
			break;
		}

		std::cout << "'" << line << "'" << " -- " << "'" << population << "'" << std::endl;
		*/

		/*** METHOD 2 ***/
		getline(inFile, line);

		inFile >> std::ws;

		if (!inFile) {
		break;
		}

		int pos = line.find(":");
		std::string country = line.substr(0, pos);
		int populationStr = stoi(line.substr(pos + 1, line.length() - 1 - pos));

		std::cout << country << " " << populationStr << std::endl;

		// Print the line
		// Remove the character at position 'pos'
		//line.erase(pos, 1);
		//std::cout << line << std::endl;
	}

	inFile.close();

	std::cin.get();
	return 0;
}