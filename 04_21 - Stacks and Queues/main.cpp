#include <iostream>
#include <stack>
#include <queue>
#include <string>

class Test {
private:
	std::string name;
public:
	Test(std::string name) : name(name) {}
	~Test() {}
	void Print() { std::cout << name << std::endl; }
};

// Queues are FIFO (First In First Out) data structures.
void Queues();
// Stacks are LIFO (Last In First Out) data structures.
void Stacks();

int main()
{
	Stacks();
	std::cout << std::endl;
	Queues();

	std::cin.get();
	return 0;
}

void Queues()
{
	std::queue<Test> testQueue;

	testQueue.push(Test("Mike"));
	testQueue.push(Test("John"));
	testQueue.push(Test("Sue"));

	// Print the element at the back of the queue.
	testQueue.back().Print();
	std::cout << std::endl;

	// Prints in order of creation, because the first element added is
	// the first "popped off".
	while (testQueue.size() > 0) {
		Test &test = testQueue.front();
		test.Print();
		testQueue.pop();
	}
}

void Stacks()
{
	std::stack<Test> testStack;

	testStack.push(Test("Mike"));
	testStack.push(Test("John"));
	testStack.push(Test("Sue"));

	// Invalid code!
	//Test &test1 = testStack.top();
	//testStack.pop(); // Object referenced to by &test1 now no longer exists.
	//test1.Print(); // Could print anything.

	// Prints in reverse order of creation, because the last element added is
	// the first "popped off".
	while (testStack.size() > 0) {
		Test &test = testStack.top();
		test.Print();
		testStack.pop();
	}
}
