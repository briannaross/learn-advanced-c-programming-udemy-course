#include <iostream>
#include <string>
#include "ring.h"

int main() {

	ring<std::string> textring(3);

	textring.add("one");
	textring.add("two");
	textring.add("three");

	for (ring<std::string>::iterator it = textring.begin(); it != textring.end(); it++) {
		std::cout << *it << std::endl;
	}

	std::cout << std::endl;

	for (std::string value : textring) {
		std::cout << value << std::endl;
	}

	std::cin.get();
	return 0;
}
