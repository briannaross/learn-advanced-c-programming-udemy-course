#include <iostream>
#include <vector>
#include <string>

int main()
{
	//std::vector<double> numbers(20, 1); // Size of 20 with every element initialised to 1.
	std::vector<double> numbers(0);

	std::cout << "Size: " << numbers.size() << std::endl;

	int capacity = numbers.capacity();
	std::cout << "Capacity: " << capacity << std::endl;

	// Capacity will increase by some system-determined amount every now and then.
	// Capacity is how much space the vector is using, not how many elements are
	// currently in the vector.
	for (int i = 0; i < 10000; i++) {
		if (numbers.capacity() != capacity) {
			capacity = numbers.capacity();
			std::cout << "Capacity: " << capacity << std::endl;
		}

		numbers.push_back(i);
	}

	numbers.clear(); // Changes the size but not capacity.
	std::cout << "Size: " << numbers.size() << std::endl;
	std::cout << "Capacity: " << numbers.capacity() << std::endl;

	numbers.resize(100); // Changes the size but not capacity.
	std::cout << "Size: " << numbers.size() << std::endl;
	std::cout << "Capacity: " << numbers.capacity() << std::endl;

	numbers.reserve(25000); // Increases the capacity but not the size.
	std::cout << "Size: " << numbers.size() << std::endl;
	std::cout << "Capacity: " << numbers.capacity() << std::endl;

	numbers.pop_back(); // Reduces the size by one but not the capacity, returns the last element.
	std::cout << "Size: " << numbers.size() << std::endl;
	std::cout << "Capacity: " << numbers.capacity() << std::endl;

	numbers.resize(5); // Resizes the vector.
	numbers.shrink_to_fit(); // Shrinks the capacity to the size of the vector.
	std::cout << "Size: " << numbers.size() << std::endl;
	std::cout << "Capacity: " << numbers.capacity() << std::endl;


	std::cin.get();
	return 0;
}
