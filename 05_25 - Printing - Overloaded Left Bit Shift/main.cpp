#include <iostream>
#include <string>

class Test {
private:
	int id;
	std::string name;
public:
	Test() : id(0), name("") {}
	Test(int id, std::string name) : id(id), name(name) {}
	// Overloaded '=' operator
	const Test& operator=(const Test &rhs) {
		id = rhs.id;
		name = rhs.name;
		return *this;
	}
	// Copy constructor
	Test(const Test &rhs) {
		*this = rhs;
	}
	// Overload '<<' (bitshift) operator. &out is the left parameter, which
	// is likely to be cout. Have to declare as a friend.
	friend std::ostream &operator<<(std::ostream &out, const Test &rhs) {
		out << rhs.id << ": " << rhs.name;
		return out;
	}
};

int main()
{
	Test test1(10, "Mike");
	Test test2(20, "Bob");

	std::cout << test1 << "\n" << test2 << std::endl;

	std::cin.get();
	return 0;
}