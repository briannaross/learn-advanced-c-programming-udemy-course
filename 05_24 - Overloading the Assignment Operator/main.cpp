#include <iostream>
#include <string>

// Rule of three: If you assign a copy constructor, or overloaded equals
// or a destructor, you should implement all three.
// If you have a destructor it's probably to clean up allocated memory,
// which means you have pointers which means you don't want to do shallow
// copies.

class Test {
private:
	int id;
	std::string name;
public:
	Test() : id(0), name("") {}
	Test(int id, std::string name) : id(id), name(name) {}
	void Print() { std::cout << id << ": " << name << std::endl; }
	// Overloaded '=' operator
	const Test& operator=(const Test &rhs) {
		std::cout << "Assignment running" << std::endl;
		id = rhs.id;
		name = rhs.name;
		return *this;
	}
	// Copy constructor
	Test(const Test &rhs) {
		std::cout << "Copy constructor running" << std::endl;
		id = rhs.id;
		name = rhs.name;
		// *this = rhs; would also work because we have the overloaded assignment operator.
	}
};

int main()
{
	Test test1(10, "Mike");
	std::cout << "Print test1: " << std::flush;
	test1.Print();

	Test test2(20, "Bob");

	test2 = test1;
	std::cout << "Print test2: " << std::flush;
	test2.Print();

	Test test3;
	//test3 = test2 = test1;

	// This is equivalent to... test3 = test2;
	test3.operator=(test2);

	std::cout << "Print test1: " << std::flush;
	test3.Print();

	std::cout << std::endl;
	// Copy initialisation (doesn't use the overloaded '=' operator).
	Test test4 = test1;
	test4.Print();


	std::cin.get();
	return 0;
}