#include <iostream>
#include "Complex.h"

using namespace caveofprogramming;

int main()
{
	Complex c1(3, 2);
	Complex c2(4, 1);

	if (c1 == c2) {
		std::cout << "Equals" << std::endl;
	}
	else {
		std::cout << "Not equal" << std::endl;
	}

	if (c1 != c2) {
		std::cout << "Not equal" << std::endl; 
	}
	else {
		std::cout << "Equals" << std::endl;
	}

	std::cin.get();
	return 0;
}
