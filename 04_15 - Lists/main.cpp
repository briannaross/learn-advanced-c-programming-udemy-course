#include <iostream>
#include <fstream>
#include <string>
#include <list>

const std::string FILENAME = "data.bin";

bool FileExists(const std::string &fName);
void ReadFile(std::list<int> &numbers, std::list<int>::iterator &it);
void WriteFile(std::list<int> &numbers, std::list<int>::iterator &it);
void Initialise(std::list<int> &numbers);
void Print(std::list<int> &numbers, std::list<int>::iterator &it);
void Delete(std::list<int> &numbers, std::list<int>::iterator &it);
void Insert(std::list<int> &numbers, std::list<int>::iterator &it);
void Exercise();
void Lecture();

// NOTE: Vector elements are next to each other in memory.
// List elements are linked to the previous and next by pointers.
// In other words, it's a doubly-linked list!

int main()
{
	Exercise();

	std::cin.get();
	return 0;
}

bool FileExists(const std::string &fName) {
	std::ifstream ifile(fName);
	return (bool)ifile;
}

// Read a list of integers from a binary file.
void ReadFile(std::list<int> &numbers, std::list<int>::iterator &it)
{
	std::fstream inFile;

	if (!FileExists(FILENAME)) {
		std::cout << FILENAME << " does not exist.\n\n";
	}
	else {
		inFile.open(FILENAME, std::ios::binary | std::ios::in);
		if (inFile.is_open()) {
			size_t numRecs = 0;
			inFile.read(reinterpret_cast<char *>(&numRecs), sizeof(size_t));
			for (size_t i = 0; i < numRecs; i++) {
				int val;
				inFile.read(reinterpret_cast<char *>(&val), sizeof(int));
				numbers.push_back(val);
			}
			inFile.close();
			std::cout << FILENAME << " read into list.\n\n";
		}
		else {
			std::cout << "Could not read file " << FILENAME << "\n\n";
		}
	}
}

// Write a list of integers out to a binary file.
void WriteFile(std::list<int> &numbers, std::list<int>::iterator &it)
{
	std::fstream outFile;
	outFile.open(FILENAME, std::ios::binary | std::ios::out);
	if (outFile.is_open()) {
		size_t size = numbers.size();
		outFile.write(reinterpret_cast<char *>(&size), sizeof(size_t));
		it = numbers.begin();
		while (it != numbers.end()) {
			outFile.write(reinterpret_cast<char *>(&*it), sizeof(int));
			it++;
		}
		outFile.close();
		std::cout << "List written to " << FILENAME << "\n\n";
	}
	else {
		std::cout << "Could not create file " << FILENAME << "\n\n";
	}
}

// Initialise a list of integers with 10 values 1-10.
void Initialise(std::list<int> &numbers)
{
	for (int i = 0; i < 10; i++) {
		numbers.push_back(i);
	}
	std::cout << "Initalised list with default values.\n\n";
}

// Print a list of integers.
void Print(std::list<int> &numbers, std::list<int>::iterator &it)
{
	int counter = 0;

	std::cout << "Displaying items in list:\n";
	while (it != numbers.end()) {
		std::cout << counter << ": " << *it << "\n";
		it++;
		counter++;
	}
	std::cout << "\n";
}

// Delete the first element from a list of integers based matching a given value.
void Delete(std::list<int> &numbers, std::list<int>::iterator &it)
{
	int val = -1;

	std::cout << "Enter a value to delete: ";
	std::cin >> val;
	while (it != numbers.end()) {
		if (*it == val) {
			it = numbers.erase(it);
			std::cout << "Value " << val << " deleted.\n\n";
			return;
		}
		else {
			it++;
		}
	}
	std::cout << "Error: Value " << val << " not found.\n\n";
}

// Insert an element into a list of integers at a given position with a given value.
void Insert(std::list<int> &numbers, std::list<int>::iterator &it)
{
	int val = -1;
	int pos = -1;
	int counter = 0;

	std::cout << "Enter a position and value to insert: ";
	std::cin >> pos >> val;
	if (pos > (int)numbers.size()) {
		std::cout << "Error: Position is greater than the size of the list.\n\n" << std::endl;
	}
	else {
		while (it != numbers.end()) {
			if (counter == pos) {
				numbers.insert(it, val);
				std::cout << "Value " << val << " inserted at position " << pos << ".\n\n";
			}
			it++;
			counter++;
		}

		if (counter == pos) {
			numbers.insert(it, val);
			std::cout << "Value " << val << " inserted at position " << pos << ".\n\n";
		}
	}
}

// This function displays a menu to allow the user to insert an element, delete an element, view the list,
// or write the list.
void Exercise() {
	std::list<int> numbers;
	std::list<int>::iterator it;

	ReadFile(numbers, it);

	if (numbers.size() <= 0) {
		Initialise(numbers);
	}

	bool quit = false;

	while (!quit) {
		char c = ' ';
		while (c != 'q' && c != '1' && c != '2' && c != '3' && c != '4') {
			std::cout << "List Menu\n=========\n1) Add Item\n2) Remove Item\n3) View Items\n4) Write Items\nq) Quit\n\nSelect option: ";
			std::cin >> c;
		}
		std::cout << "\n";

		it = numbers.begin();

		switch (c) {
		case '1':
			Insert(numbers, it);
			break;
		case '2':
			Delete(numbers, it);
			break;
		case '3':
			Print(numbers, it);
			break;
		case '4':
			WriteFile(numbers, it);
			break;
		case 'q':
			quit = true;
			break;
		}

		c = ' ';
	}
}

// Stuff the lecturer showed.
void Lecture() {
	std::list<int> numbers;

	numbers.push_back(1);
	numbers.push_back(2);
	numbers.push_back(3);
	numbers.push_front(0);

	std::list<int>::iterator it = numbers.begin();
	it++;
	numbers.insert(it, 100); // This inserts an element before the element pointed to by the iterator.
	std::cout << "Element: " << *it << std::endl;

	std::list<int>::iterator eraseIt = numbers.begin();
	eraseIt++;
	//numbers.erase(eraseIt); // Can no longer dereference the iterator because it points to nothing.
	eraseIt = numbers.erase(eraseIt); // Erase returns an iterator to the previous element, so do this instead of the previous line.
	std::cout << "Element: " << *eraseIt << std::endl;

	for (std::list<int>::iterator it = numbers.begin(); it != numbers.end(); ) { /*it++) {*/ // Note: Can't use it+=2 or some other value as with vectors.
																							 // Insert 1234 before any element that has the value 2
		if (*it == 2) {
			numbers.insert(it, 1234);
		}

		if (*it == 1) {
			it = numbers.erase(it); // This will fail because it will invalidate the iterator (it will skip the next element in the loop)...
		}
		else {
			it++; // ...so do this so the iterator doesn't increment.
		}
	}

	for (std::list<int>::iterator it = numbers.begin(); it != numbers.end(); it++) { // Note: Can't use it+=2 or some other value as with vectors.
		std::cout << *it << std::endl;
	}
}