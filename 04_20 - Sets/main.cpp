#include <iostream>
#include <set>
#include <string>

class Widget {
private:
	std::string name;
	int id;
public:
	Widget() : name("UNDEFINED"), id(-1) {}
	Widget(std::string name, int num) : name(name), id(num) {}
	void Print() const {
		std::cout << name << ", " << id;
	}
	bool operator< (const Widget &rhs) const {
		if (name == rhs.name) {
			return id < rhs.id;
		}
		else {
			return name < rhs.name;
		}
	}
};

int main()
{
	// Sets contain only unique elements.
	std::set<int> numbers;
	numbers.insert(1);
	numbers.insert(2);
	numbers.insert(3);
	numbers.insert(4);
	numbers.insert(5);
	numbers.insert(1);
	
	for (auto it = numbers.begin(); it != numbers.end(); it++) {
		std::cout << *it << std::endl;
	}
	std::cout << std::endl;

	auto itFind = numbers.find(5);
	if (itFind != numbers.end()) {
		std::cout << "Found: " << *itFind << std::endl;
	}
	std::cout << std::endl;

	// Returns the number of elements with key that compares equivalent to the value,
	// which is either 1 or 0 since sets don't allow duplicates. 
	if (numbers.count(3)) {
		std::cout << "Number found." << std::endl;
	}
	std::cout << std::endl;




	std::set<Widget> widgets;
	widgets.insert(Widget("Bob", 1));
	widgets.insert(Widget("Jenny", 2));
	widgets.insert(Widget("Mike", 3));
	widgets.insert(Widget("Wendy", 4));
	widgets.insert(Widget("Alice", 5));
	widgets.insert(Widget("Alice", 6));
	widgets.insert(Widget());

	for (auto it = widgets.begin(); it != widgets.end(); it++) {
		(*it).Print();
		std::cout << std::endl;
	}
	std::cout << std::endl;


	std::cin.get();
	return 0;
}