#include <iostream>
#include <map>
#include <string>

int main()
{
	std::map<std::string, int> ages;

	// Add values to the map.
	// Overloaded [] operator where we can store the key, then assign a value to it.
	ages["Mike"] = 40;
	ages["Raj"] = 20;
	ages["Vicky"] = 30;

	ages["Mike"] = 70;

	//// Insert a pair into the map...
	//std::pair<std::string, int> addToMap("Peter", 100);
	//ages.insert(addToMap);

	//// ...or insert like this...
	//ages.insert(std::pair<std::string, int>("Peter", 100));

	// ...or even like this.
	ages.insert(std::make_pair("Peter", 100));

	// Pair gets created if it doesn't exist.
	std::cout << ages["Raj"] << std::endl;

	// Find a key in the map.
	if (ages.find("Vicky1") != ages.end()) {
		std::cout << "Found Vicky" << std::endl;
	}
	else {
		std::cout << "Key not found" << std::endl;
	}

	std::cout << std::endl;
	// Output all values in the map by obtaining each pair and outputting values by pair.
	for (std::map<std::string, int>::iterator it = ages.begin(); it != ages.end(); it++) {
		std::pair<std::string, int> age = *it;
		std::cout << age.first << ": " << age.second << std::endl;
	}

	std::cout << std::endl;
	// Output all values in the map by using the iterator.
	for (std::map<std::string, int>::iterator it = ages.begin(); it != ages.end(); it++) {
		std::cout << it->first << ": " << it->second << std::endl;
	}

	std::cin.get();
	return 0;
}