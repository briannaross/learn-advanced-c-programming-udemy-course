#include <iostream>
#include <algorithm>
#include <vector>

bool match(std::string test) {
	return test.size() == 3;
}

// Second parameter is a pointer to a function, in this case, bool match(string).
int countStrings(std::vector<std::string> &texts, bool(*match)(std::string test)) {
	int count = 0;
	for (auto it = texts.begin(); it != texts.end(); it++) {
		if (match(*it))
			count++;
	}
	return count;
}

int main()
{
	std::vector<std::string> texts;
	texts.push_back("one");
	texts.push_back("two");
	texts.push_back("three");
	texts.push_back("two");
	texts.push_back("four");
	texts.push_back("two");
	texts.push_back("three");

	std::cout << match("one") << std::endl;
	
	// count_if() is a function in the algorithm library.
	// Last parameter is a function that returns a bool.
	std::cout << count_if(texts.begin(), texts.end(), match) << std::endl;

	std::cout << countStrings(texts, match) << std::endl;

	std::cin.get();
	return 0;
}
