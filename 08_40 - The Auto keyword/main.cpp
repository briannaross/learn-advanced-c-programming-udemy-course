#include <iostream>
#include <string>

////auto test() -> int { // C++11
//auto test() { // C++14 (>gcc 4.8.0)
//	return 100;
//}

template <class T, class S>
// auto test(T value) -> decltype(value) { // C++11
auto test(T value1, S value2) { // C++14 (>gcc 4.8.0)
		return value1 + value2;
}

int get() {
	return 999;
}

auto test2() {
	return get();
}

int main() {

	// Pre C++11 (at least in GNU compilers)
	//auto int value = 7;

	// C++11
	auto value = 7;
	auto text = "Hello";
	//std::cout << value << std::endl;
	//std::cout << text << std::endl;

	//std::cout << test(5, 6) << std::endl;

	std::cout << test2() << std::endl;

	std::cin.get();
	return 0;
}
