#include <iostream>

// Effectively gets rid of the padding in structs (don't know what else it does.)
// The 1 forces the program to align the data on single-byte boundaries.
#pragma pack(push, 1)

struct Person {
	// 64 bytes with default packing.
	// 62 bytes without packing.
	char name[50];
	int age;
	double weight;
};

// Do this to set the packing back to default(?)
#pragma pack(pop)

int main()
{
	std::cout << sizeof(Person) << std::endl;

	std::cin.get();
	return 0;
}