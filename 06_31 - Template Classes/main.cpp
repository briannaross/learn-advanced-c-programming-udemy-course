#include <iostream>
#include <string>

template <class T, class K>
class Test
{
private:
	T objT;
	K objK;
public:
	Test(T obj, K obj2) : objT(obj), objK(obj2) {}
	void Print() {
		std::cout << objK << ": " << objT << std::endl;
	}
};

int main()
{
	Test<std::string, int> test1("Hello", 1);
	test1.Print();

	Test<int, int> test2(123, 0);
	test2.Print();

	Test<bool, bool> test3(true, false);
	test3.Print();

	std::cin.get();
	return 0;
}
