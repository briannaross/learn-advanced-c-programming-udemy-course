#include <iostream>
#include <map>
#include <string>

class Person {
private:
	std::string name;
	int age;
public:
	Person() : name(""), age(0) {}
	Person(std::string name, int age) : name(name), age(age) {}

	// Copy constructor
	Person(const Person &other) {
		std::cout << "Copy constructor running!" << std::endl;
		name = other.name;
		age = other.age;
	}
	void print() {
		std::cout << name << ": " << age << std::endl;
	}
};

int main()
{
	std::map<int, Person> people;

	// Calls default constructor
	people.insert(std::make_pair(56, Person()));

	// Call user-defined constructor
	people[50] = Person("Mike", 40);
	people[32] = Person("Raj", 20);
	people[1] = Person("Vicky", 30);

	// Calls copy constructor
	people.insert(std::make_pair(55, Person("Bob", 45)));
	people.insert(std::make_pair(55, Person("Sue", 24))); // Won't overwrite Bob...
	people[55] = Person("Sue", 24); // ...but this will.


	for (std::map<int, Person>::iterator it = people.begin(); it != people.end(); it++) {
		std::cout << it->first << ": " << std::flush;
		it->second.print();
	}

	std::cin.get();
	return 0;
}