#include <iostream>
#include <typeinfo> // Include when using typeid (at least for GNU compilers)
#include <string>


int main() {

	std::string value;

	// This is referred to as "name mangling".
	std::cout << typeid(value).name() << std::endl;
	std::cout << std::endl;

	// C++11 - Declares a new variable of the same type as the variable provided.
	decltype(value) name = "Bob";
	std::cout << "Bob" << std::endl;
	std::cout << typeid(name).name() << std::endl;

	std::cin.get();
	return 0;
}
