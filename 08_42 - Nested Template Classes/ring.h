#pragma once
#include <iostream>

template <typename T>
class ring {
public:
	class iterator; // Makes iterator a subclass of ring
};

template <typename T>
class ring<T>::iterator {
public:
	void print() {
		std::cout << "Hello from iterator: " << T() << std::endl;
	}
};

