#pragma once

#include <iostream>

namespace caveofprogramming {

class Complex
{
private:
	double real;
	double imaginary;
public:
	Complex();
	Complex(double real, double imaginary);
	Complex(const Complex &rhs);
	const Complex &operator=(const Complex &rhs);
	//const Complex &operator+(const Complex &rhs);
	double getReal() const { return real; }
	double getImaginary() const { return imaginary; }

};

std::ostream &operator<<(std::ostream &out, const Complex &rhs);

}