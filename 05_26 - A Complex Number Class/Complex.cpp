#include "Complex.h"

namespace caveofprogramming {

	std::ostream &operator<<(std::ostream &out, const Complex &rhs)
	{
		out << "(" << rhs.getReal() << ", " << rhs.getImaginary() << ")";
		return out;
	}


	Complex::Complex() : real(0), imaginary(0)
	{
	}

	Complex::Complex(double real, double imaginary) : real(real), imaginary(imaginary)
	{

	}

	Complex::Complex(const Complex &rhs)
	{
		real = rhs.real;
		imaginary = rhs.imaginary;
	}

	const Complex &Complex::operator=(const Complex &rhs)
	{
		real = rhs.real;
		imaginary = rhs.imaginary;
		return *this;
	}

	//const Complex &Complex::operator+(const Complex &rhs)
	//{
	//	Complex c(real + rhs.real, imaginary + rhs.imaginary);
	//	return c;
	//}

}