#include <iostream>

class CanGoWrong
{
public:
	CanGoWrong() {
		__int64 dl = 999999999999999999;
		char *pMemory = new char[dl];
		delete[] pMemory;
	}
};

int main()
{
	try {
		CanGoWrong wrong;
	}
	catch (std::bad_alloc &e) {
		std::cout << "Caught exception: " << e.what() << std::endl;
	}

	std::cout << "Still running..." << std::endl;

	std::cin.get();

	return 0;
}