#include <iostream>

class MyObject{
private:
	int num;
public:
	MyObject(int num) : num(num) {};
	friend std::ostream &operator<<(std::ostream &out, MyObject &rhs);
};

std::ostream &operator<<(std::ostream &out, MyObject &rhs) {
	out << rhs.num;
	return out;
}

void test(int value) {
	std::cout << "Hello: " << value << std::endl;
}

void Print(MyObject &obj)
{
	std::cout << obj << std::endl;
}

int main()
{
	test(1);

	// void (*pTest)(); // Declare the function pointer with the same return type and parameter list as the function to be called.
	void(*pTest)(int) = test; // Can do this too.

	// pTest = &test; // Get a reference to the function to be called.
	// pTest = test; // ... but we can just do this instead.

	// (*pTest)(); // Call the function...
	pTest(123); // ...but this is nicer.


	MyObject obj1(111);
	void(*pTest2)(MyObject&) = Print;
	pTest2(obj1);




	std::cin.get();
	return 0;
}
