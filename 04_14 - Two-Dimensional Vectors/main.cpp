#include <iostream>
#include <vector>

int main()
{
	/*
	// Initialise a 2D vector of 3 rows with a 4 element vector in each.
	// Each element in set to 7.
	std::vector< std::vector<int> > grid(3, std::vector<int>(4, 7)); // Note the spacing between the outermost < and >.

	grid[1].push_back(8); // Push an element onto the back of the second vector (row) within grid.

	// Iterate through the outermost and innermost vectors, then use the
	// overloaded [][] to access the value.
	for (int row = 0; row < grid.size(); row++) {
		for (int col = 0; col < grid[row].size(); col++) {
			std::cout << grid[row][col] << std::flush;
		}
		std::cout << std::endl;
	}
	*/

	std::vector< std::vector<int> > grid(12, std::vector<int>(12)); // 12x12 grid
	// Multiply row by col, effectively creating a times table.
	for (size_t row = 0; row < grid.size(); row++) {
		for (size_t col = 0; col < grid[row].size(); col++) {
			grid[row][col] = (row + 1) * (col + 1);
		}
	}

	// Output said times table.
	for (size_t row = 0; row < grid.size(); row++) {
		for (size_t col = 0; col < grid[row].size(); col++) {
			std::cout << grid[row][col] << "\t" << std::flush;
		}
		std::cout << std::endl;
	}

	std::cin.get();
	return 0;
}