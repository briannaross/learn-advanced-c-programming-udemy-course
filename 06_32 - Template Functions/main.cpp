#include <iostream>
#include <string>

class Widget
{
private:
	int num;
	std::string identifier;
public:
	Widget() : num(0), identifier("") {}
	Widget(int num, std::string identifier) : num(num), identifier(identifier) {}
	friend std::ostream &operator<<(std::ostream &out, const Widget &rhs);
};

std::ostream &operator<<(std::ostream &out, const Widget &rhs) {
	out << rhs.num << ": " << rhs.identifier;
	return out;
}

template<class T>
//template <typename T> // Can also use this.
void print(T obj) {
	std::cout << obj << std::endl;
}

int main()
{
	print<std::string>("Hello");
	print<int>(5);
	print("Hi there");

	Widget w1(1, "Banana");
	Widget w2(2, "Computer");
	Widget w3(3, "Enigma");

	print(w1);

	std::cin.get();
	return 0;
}