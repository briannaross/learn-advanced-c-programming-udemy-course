#include <iostream>
#include <map>
#include <string>

int main()
{
	std::multimap<int, std::string> lookup;

	lookup.insert(std::make_pair(30, "Mike"));
	lookup.insert(std::make_pair(10, "Vicky"));
	lookup.insert(std::make_pair(30, "Raj"));
	lookup.insert(std::make_pair(20, "Bob"));

	for (std::multimap<int, std::string>::iterator it = lookup.begin(); it != lookup.end(); it++) {
		std::cout << it->first << ": " << it->second << std::endl;
	}
	std::cout << std::endl;

	// Outputs everything from the first found instance of value.
	for (std::multimap<int, std::string>::iterator it = lookup.find(20); it != lookup.end(); it++) {
		std::cout << it->first << ": " << it->second << std::endl;
	}
	std::cout << std::endl;

	// Return a pair of iterators where each iterator points to the first and second instances of value found.
	// If only one value is found then its.first and its.last will be the same.
	// Can be used to output values from one iterator to the other.
	std::pair<std::multimap<int, std::string>::iterator, std::multimap<int, std::string>::iterator> its = lookup.equal_range(20);
	for (std::multimap<int, std::string>::iterator it = its.first; it != its.second; it++) {
		std::cout << it->first << ": " << it->second << std::endl;
	}
	std::cout << std::endl;

	// C++11 version of above. Much nicer!
	auto its2 = lookup.equal_range(20);
	for (auto it = its2.first; it != its2.second; it++) {
		std::cout << it->first << ": " << it->second << std::endl;
	}
	std::cout << std::endl;


	std::cin.get();
	return 0;
}