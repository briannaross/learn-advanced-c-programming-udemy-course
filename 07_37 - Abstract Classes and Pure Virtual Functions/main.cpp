#include <iostream>

class Animal
{
public:
	virtual void run() = 0;
	virtual void speak() = 0; // Pure virtual function, this is what makes a class abstract (it cannot be directly instantiated).
};

// Dog is abstract because run() isn't implemented.
class Dog : public Animal
{
public:
	virtual void speak() {
		std::cout << "Woof!" << std::endl;
	}

};

// Labrador contains an implementation of every virtual function, because although run() isn't implemented here,
// it is implemented in its' parent.
class Labrador : public Dog {
public:
	Labrador() {
		std::cout << "New Labrador" << std::endl;
	}

	virtual void run() {
		std::cout << "Labrador running..." << std::endl;
	}

};

void test(Animal &a) {
	a.run();
}

int main()
{
	//Labrador labs[5];

	Labrador lab;
	lab.run();
	lab.speak();

	// Can create pointers to a pure virtual class and then use the references of derived classes to access them.
	Animal *animals[5];
	animals[0] = &lab;

	animals[0]->speak();

	// Works because Labrador is a type of Animal.
	test(lab);

	std::cin.get();
	return 0;
}