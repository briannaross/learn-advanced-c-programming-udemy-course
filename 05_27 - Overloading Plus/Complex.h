#pragma once

#include <iostream>

// Three ways of doing operator overloading:
// * Member function
// * Friend function
// * Normal function
//
// Some general rules of thumb (RoTs) from http://www.learncpp.com/cpp-tutorial/94-overloading-operators-using-member-functions/
// 1) If you�re overloading assignment(= ), subscript([]), function call(()), or member selection(->), do so as a member function.
// 2) If you�re overloading a unary operator, do so as a member function.
// 3) If you�re overloading a binary operator that modifies its left operand(e.g. operator+=), do so as a member function if you can.
// 4) If you�re overloading a binary operator that does not modify its left operand(e.g. operator+), do so as a normal function (a) or friend function (b).
// ...though bear in mind that this is not gospel.


namespace caveofprogramming {

class Complex
{
private:
	double real;
	double imaginary;
public:
	Complex();
	Complex(double real, double imaginary);
	Complex(const Complex &rhs);
	const Complex &operator=(const Complex &rhs); // RoT 1
	double getReal() const { return real; }
	double getImaginary() const { return imaginary; }
	friend Complex operator-(const Complex &lhs, const Complex &rhs); // Rot 4b
	Complex operator-(double rhs); // Does not go according to RoT

};

std::ostream &operator<<(std::ostream &out, const Complex &rhs);
Complex operator+(const Complex &lhs, const Complex &rhs); // Rot 4a
Complex operator+(const Complex &lhs, double d); // Rot 4a
Complex operator+(double d, const Complex &rhs); // Rot 4a
Complex operator-(double d, const Complex &rhs); // Rot 4a

}