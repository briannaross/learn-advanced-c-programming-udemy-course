#include <iostream>
#include <fstream>
#include <string>

// Effectively gets rid of the padding in structs (don't know what else it does.)
// The 1 forces the program to align the data on single-byte boundaries.
#pragma pack(push, 1)

struct Person {
	// 64 bytes with default packing.
	// 62 bytes without packing.
	char name[50];
	int age;
	double height;
};

// Do this to set the packing back to default(?)
#pragma pack(pop)

int main()
{
	std::string fileName = "test.bin";

	/*** WRITE BINARY FILE ***/
	Person person = { "Eddie", 39, 174 };
	std::fstream outFile;
	// Open file for writing in binary mode.
	// Specify std::ios::out for writing output (not neccesary with a file of type ofstream).
	outFile.open(fileName, std::ios::binary|std::ios::out);
	if (outFile.is_open()) {
		//outFile.write((char *)&person, sizeof(person)); // Old-school
		outFile.write(reinterpret_cast<char *>(&person), sizeof(Person)); // Apparently this is better.
		outFile.close();
	}
	else {
		std::cout << "Could not create file " << fileName << std::endl;
	}


	/*** READ BINARY FILE ***/
	Person person2 = {};
	std::fstream inFile;
	// Open file for reading in binary mode.
	// Specify std::ios::in for reading input (not neccesary with a file of type ifstream).
	inFile.open(fileName, std::ios::binary | std::ios::in);
	if (inFile.is_open()) {
		inFile.read(reinterpret_cast<char *>(&person2), sizeof(Person));
		inFile.close();
	}
	else {
		std::cout << "Could not read file " << fileName << std::endl;
	}
	std::cout << person2.name << ", " << person2.age << ", " << person2.height << std::endl;

	std::cin.get();
	return 0;
}