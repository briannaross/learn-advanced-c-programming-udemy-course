#include <iostream>
#include <fstream>
#include <string>

int main()
{
	std::string inFileName = "test.txt";
	
	//std::ifstream inFile;
	std::fstream inFile;

	//inFile.open(inFileName); // Could do this, or we could be more explicit with the fstream type below
	inFile.open(inFileName, std::ios::in);

	if (inFile.is_open()) {

		std::string line;

		// while (!inFile.eof()) { // Can use this, but...
		while (inFile) { // Can do this because the bool operator has been overloaded for eof!
			std::getline(inFile, line);
			std::cout << line << std::endl;
		}

		inFile.close();
	}
	else {
		std::cout << "Cannot open file: " << inFileName << std::endl;
	}

	std::cin.get();
	return 0;
}