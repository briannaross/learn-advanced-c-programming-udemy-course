#include <iostream>
#include <vector>
#include <map>
#include <string>

// This lecture discusses embedding STL data types within other STL data types.

int main()
{
	std::map<std::string, std::vector<int> > scores;

	scores["Mike"].push_back(10);
	scores["Mike"].push_back(20);
	scores["Vicky"].push_back(15);

	for (auto it = scores.begin(); it != scores.end(); it++) {
		std::string name = it->first;
		std::vector<int> scoreList = it->second;

		std::cout << name << ": " << std::flush;

		for (size_t i = 0; i < scoreList.size(); i++) {
			std::cout << scoreList[i] << " " << std::flush;
		}
		std::cout << std::endl;
	}

	std::cin.get();
	return 0;
}