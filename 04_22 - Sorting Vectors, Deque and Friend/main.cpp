#include <iostream>
#include <vector>
#include <deque>
#include <string>
#include <algorithm>

class Test {
private:
	int id;
	std::string name;
public:
	Test (int id, std::string name) : id(id), name(name) {}
	void Print() { std::cout << id << ": " << name << std::endl; }
	//bool operator<(const Test &other) const {
	//	return name < other.name;
	//}

	// friend allows comp() to access the members of the class without the
	// need for getters.
	friend bool comp(const Test &a, const Test &b);
};

bool comp(const Test &a, const Test &b)
{
	return a.name < b.name;
}

int main()
{
	std::vector<Test> tests;
	tests.push_back(Test(5, "Mike"));
	tests.push_back(Test(10, "Sue"));
	tests.push_back(Test(7, "Raj"));
	tests.push_back(Test(3, "Vicky"));
	sort(tests.begin(), tests.end(), comp);
	for (int i = 0; i < tests.size(); i++) {
		tests[i].Print();
	}
	std::cout << std::endl;

	/*
	From cppreference.com
	std::deque (double-ended queue) is an indexed sequence container that allows fast insertion and deletion at both its beginning and its end.
	In addition, insertion and deletion at either end of a deque never invalidates pointers or references to the rest of the elements.

	As opposed to std::vector, the elements of a deque are not stored contiguously: typical implementations use a sequence of individually
	allocated fixed-size arrays, with additional bookkeeping, which means indexed access to deque must perform two pointer dereferences,
	compared to vector's indexed access which performs only one.

	The storage of a deque is automatically expanded and contracted as needed. Expansion of a deque is cheaper than the expansion of a std::vector
	because it does not involve copying of the existing elements to a new memory location. On the other hand, deques typically have large minimal
	memory cost; a deque holding just one element has to allocate its full internal array (e.g. 8 times the object size on 64-bit libstdc++; 16
	times the object size or 4096 bytes, whichever is larger, on 64-bit libc++).
	*/

	// Deques (Double-Ended QUEue) allow for push_back and push_front.
	std::deque<Test> tests2;
	tests2.push_back(Test(5, "Mike"));
	tests2.push_front(Test(10, "Sue"));
	tests2.push_back(Test(7, "Raj"));
	tests2.push_front(Test(3, "Vicky"));
	for (int i = 0; i < tests2.size(); i++) {
		tests2[i].Print();
	}
	std::cout << std::endl;
	// Can also pop the front and back elements.
	tests2.pop_front();
	tests2.pop_back();
	for (int i = 0; i < tests2.size(); i++) {
		tests2[i].Print();
	}

	std::cin.get();
	return 0;
}