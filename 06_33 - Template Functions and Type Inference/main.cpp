#include <iostream>
#include <string>

template<class T>
void print(T obj) {
	std::cout << "Template version: " << obj << std::endl;
}

void print(int value) {
	std::cout << "Non-template version: " << value << std::endl;
}

// No arguments so compiler can't infer type.
template<class T>
void show() {
	std::cout << T() << std::endl;
}


int main()
{
	print<std::string>("Hello");
	print<int>(5);

	print("Hi there");

	print(5); // Calls the non-template version.
	print<>(6); // Infers that we want the template version of print().

	//show(); Won't work.
	//show<>(); Also won't work.
	show<int>(); // Works but why would you want to do this?!

	std::cin.get();
	return 0;
}
