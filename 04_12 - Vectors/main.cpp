#include <iostream>
#include <vector>
#include <string>

int main()
{
	//std::vector<std::string> strings(5); // Presizing the vector.
	//strings[10] = "thing"; // Won't work here as there's only 5 elements.

	std::vector<std::string> strings;
	// strings[10] = "thing"; // Won't work here either, at least while there's not an 11th element.

	// Add elements to the end of the vector.
	strings.push_back("one");
	strings.push_back("two");
	strings.push_back("three");

	//std::cout << strings[1] << std::endl; // [] operator is overloaded to act like an array.
	//std::cout << strings.size() << std::endl;

	// Loops through the vector, but not the recommended way to do it.
	//for (size_t i = 0; i < strings.size(); i++) {
	//	std::cout << strings[i] << std::endl;
	//}

	// Create an iterator (which is a pointer).
	//std::vector<std::string>::iterator it = strings.begin();
	// Output the first element with the iterator set to the beginning of the vector.
	//std::cout << *it << std::endl;

	// Increment is overloaded so as to point to the next item in the vector.
	//it++;
	//std::cout << *it << std::endl;

	// Iterate through the vector using an iterator.
	for (std::vector<std::string>::iterator it = strings.begin(); it != strings.end(); it++) {
		std::cout << *it << std::endl;
	}

	std::cin.get();
	return 0;
}