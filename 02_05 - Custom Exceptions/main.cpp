#include <iostream>
#include <exception>

class MyException : public std::exception
{
public:
	virtual const char* what() const throw() {
		return "Something bad happened.";
	}
};

class Test {
public:
	void GoesWrong() {
		throw MyException();
	}
};

int main()
{
	Test test;
MyException myexc;
	try {
		test.GoesWrong();
	}
	catch (MyException &e) {
		std::cout << e.what() << std::endl;
	}

	std::cin.get();

	return 0;
}
