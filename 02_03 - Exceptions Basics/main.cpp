#include <iostream>

struct Widget {
	int num;
	std::string desc;
};

void mightGoWrong()
{
	Widget w1;
	w1.num = 1;
	w1.desc = "The first widget of widgetiness.";

	bool error1 = false;
	bool error2 = false;

	if (error1) {
		throw "Something went wrong.";
	}

	if (error2) {
		throw std::string("Something else went wrong.");
	}
}

void usesMightGoWrong()
{
	mightGoWrong();
}


int main()
{
	try {
		usesMightGoWrong();
	}
	catch (int e) {
		std::cout << "Error code: " << e << std::endl;
	}
	catch (char const* e) {
		std::cout << "Error message: " << e << std::endl;
	}
	catch (std::string &e) {
		std::cout << "String error message: " << e.data() << std::endl;
	}
	catch (Widget &e) {
		std::cout << "Widget error, widget #" << e.num << std::endl;
	}

	std::cout << "Still running..." << std::endl;

	std::cin.get();

	return 0;
}