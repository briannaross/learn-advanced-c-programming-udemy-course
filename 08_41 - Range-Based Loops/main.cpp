#include <iostream>
#include <list>
#include <vector>

int main() {

	std::string s1 = "abc";
	std::string s2 = "def";
	auto s0 = { s1, s2 };
	std::cout << typeid(s0).name() << std::endl << std::endl << std::endl;

	// The old way
	std::list<char const *> oldtexts = { "One", "Two", "Three" };
	for (std::list<char const *>::iterator it = oldtexts.begin(); it != oldtexts.end(); it++) {
		std::cout << *it << std::endl;
	}
	std::cout << typeid(oldtexts).name() << std::endl << std::endl;

	
	// C++11 way - fucking awesome!!!
	auto texts = { "One", "Two", "Three" };
	for (auto text : texts) {
		std::cout << text << std::endl;
	}
	std::cout << typeid(texts).name() << std::endl << std::endl;

	std::vector<int> numbers;
	numbers.push_back(5);
	numbers.push_back(7);
	numbers.push_back(9);
	numbers.push_back(11);
	for (auto number : numbers) {
		std::cout << number << std::endl;
	}
	std::cout << std::endl;

	std::string hello = "Hello";
	for (auto c : hello) {
		std::cout << c << std::endl;
	}
	std::cout << std::endl;


	std::cin.get();
	return 0;
}
