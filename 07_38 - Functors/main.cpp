#include <iostream>
#include <string>

struct Test {
	// Pure virtual function
	virtual bool operator()(std::string &text) = 0;
	virtual ~Test() {}
};

struct MatchTest : public Test {
	// Functor - overloads the () operator.
	virtual bool operator()(std::string &text) {
		return text == "lion";
	}
};

void check(std::string text, Test &test) {
	if (test(text)) {
		std::cout << "Text matches!" << std::endl;
	}
	else {
		std::cout << "No match." << std::endl;
	}
}

int main() {
	MatchTest pred;

	std::string value = "lion";

	MatchTest m;
	check("lion", m);

	std::cin.get();
	return 0;
}