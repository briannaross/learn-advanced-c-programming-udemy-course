#include <iostream>

class Parent {
private:
	int one;
public:
	Parent(): one(0) {
	}

	Parent(const Parent &other): one(0) {
		one = other.one;
		std::cout << "Copy Parent" << std::endl;
	}

	virtual void print() {
		std::cout << "Parent" << std::endl;
	}
};

class Child : public Parent {
private:
	int two;
public:
	Child(): two(0) {

	}

	void print() {
		std::cout << "Child" << std::endl;
	}
};

int main()
{
	// If Parent.print() is not virtual then it calls Parent.print(), because there is no lookup mechanism
	// for the compiler to find Child.print().
	// If Parent.print() is virtual then it calls Child.print().
	Child c1;
	Parent &p1 = c1;
	p1.print(); 

	// This calls the Parent class copy constructor.
	Parent p2 = Child();
	p2.print();

	std::cin.get();
	return 0;
}